# Digit Recognition API (digitreg-api)

TODO: This will be an small Flask app to experiment with providing
an API for a digit recognition service. The actual digit recognition
code will live elsewhere: this repository just contains the API module.

## Some early ideas and principles

### Adopted

#### Use `flask-restful`

#### Use `flask-restful-swagger`

Status: a simple PoC implemented.

#### TODO: My Next Great idea

### Abandoned

### Use `jsonapi.org`

Considered for a while, ended up not going this route, at least not in the beginning. 

Will use simpler custom JSON structures instead. This is partly due to a mismatch between `jsonapi` and the default 
marshalling provided by `flask-restful`.



