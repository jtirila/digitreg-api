from flask import Flask
from flask_restful import Api, Resource, fields, marshal_with
from flask_restful_swagger import swagger
app = Flask(__name__)

api = swagger.docs(Api(app), apiVersion='0.0.1')


class RecognitionResponse(object):
    response_fields = {'digit': fields.Integer, 'method': fields.String}

    def __init__(self):
        self.type = "recognition_response"
        self.method = "wild guess"
        self.digit = 6


class RecognitionRequest(Resource):
    """My Recognition Service Description, or something like that."""
    @marshal_with(RecognitionResponse.response_fields)
    @swagger.operation(
        notes='some really good notes',
        responseClass=RecognitionResponse.__name__,
        nickname='upload',
        parameters=[
            {
              "name": "body",
              "description": "FIXME: image data in some format",
              "required": True,
              "allowMultiple": False,
              "dataType": "FIXME: unknown",
              "paramType": "body"
            }
          ],
        responseMessages=[
            {
              "code": 201,
              "message": "Created. The URL of the created blueprint should be in the Location header"
            },
            {
              "code": 405,
              "message": "Invalid input"
            }
          ]
        )
    def post(self):
        return RecognitionResponse()


api.add_resource(RecognitionRequest, '/recognition_requests/')

